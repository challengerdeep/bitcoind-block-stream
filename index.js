var inherits = require('inherits')
var Readable = require('readable-stream').Readable

function getBlock (client, height, cb) {
  client.getBlockHash(height, function (err, hash) {
    client.getBlock(hash, cb)
  })
}

inherits(BlockStream, Readable)

function BlockStream (client, opts) {
  if (!client || !client.getBlock)
    return new Error('I need a bitcoin client')

  if (!(this instanceof BlockStream))
    return new BlockStream(opts)


  opts = opts || {}
  opts.objectMode = true

  Readable.call(this, opts)

  this._client = client
  this._height = opts.from || 0
  this._to = opts.to
}

BlockStream.prototype._read = function () {
  if (this._to && this._height > this._to)
    this.push(null)
  else {
    var self = this
    getBlock(this._client, this._height++, function (err, block) {
      if (err) {
        // we are up to date, end the stream
        self.push(null)
      } else {
        self.push(block)
      }
    })
  }
}

module.exports = function blockStream (client, opts) {
  return new BlockStream(client, opts)
}
